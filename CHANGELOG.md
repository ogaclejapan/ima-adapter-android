## [6.0.9] - 2018-07-31
### Fixed
- Now when AdsManager/StreamManager is null, the application won't crash anymore.

## [6.0.8] - 2018-05-24
### Added
 - Sample project for IMA DAI
### Fixed
 - Now for live ads all is considered midroll

## [6.0.7] 2018-04-23
### Removed
 - Ad pause / Ad resume are not send anymore on DAI to keep it consistent with iOS

## [6.0.6] 2018-04-17
### Fixed
 - Join time was not send in some cases

## [6.0.5] 2018-04-16
### Added
 - Support for DAI

## [6.0.4] 2018-03-15
### Added
 - Support for ExoPlayer IMA extension (for more information check documentation)

## [6.0.3] - 2017-12-15
### Added
 - Ad served metric
 - Ad Adapter version
 - FireAllAdsCompleted call
 - Stop if all ads completed
 
## [6.0.1] - 2017-10-10
### Added
 - AdInit added
 
## [6.0.0] - 2017-10-10
### Added
 - Release version
