package com.npaw.ima;

import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManager;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;

import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * Created by Enrique on 28/09/2017.
 */

public class ImaAdapter extends PlayerAdapter<AdsManager> implements AdErrorEvent.AdErrorListener,
        AdEvent.AdEventListener {

    private String adTag;
    private AdPosition lastPosition;
    private AdsLoader loader;
    private boolean adServed;

    /**
     * Constructor.
     * When overriding it, make sure to call super.
     * Implement the logic to register to the player events in the {@link #registerListeners()} method.
     * Usually you will want to call {@link #registerListeners()} from the overridden constructor.
     *
     * @param player the player instance this Adapter will be bounded to.
     */
    public ImaAdapter(AdsManager player) {
        super(player);

        registerListeners();
        monitorPlayhead(true,false,800);
        adServed = false;
    }

    public ImaAdapter(AdsLoader loader) {
        super(null);

        this.loader = loader;
        this.loader.addAdsLoadedListener(new AdsLoader.AdsLoadedListener() {
            @Override
            public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {
                setPlayer(adsManagerLoadedEvent.getAdsManager());
            }
        });
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        if(getPlayer() == null) return;

        getPlayer().addAdErrorListener(this);
        getPlayer().addAdEventListener(this);
    }

    private String getUrl() {
        if (getPlayer().getCurrentAd() != null) {
            try {
                Field f = getPlayer().getCurrentAd().getClass().
                        getDeclaredField("clickThroughUrl");
                f.setAccessible(true);
                return f.get(getPlayer().getCurrentAd()).toString();
            } catch (Exception e) {
                YouboraLog.error(e);
            }
        }
        return null;
    }

    @Override
    public void unregisterListeners() {
        super.unregisterListeners();

        getPlayer().removeAdErrorListener(this);
        getPlayer().removeAdEventListener(this);
    }

    @Override
    public Double getPlayhead() {
        return getPlayer().getCurrentAd() == null ? null : (double)getPlayer().getAdProgress().getCurrentTime();
    }

    @Override
    public Double getDuration() {
        return getPlayer().getCurrentAd() == null ? null :  getPlayer().getCurrentAd().getDuration();
    }

    @Override
    public String getTitle() {
        return getPlayer().getCurrentAd() == null ? null : getPlayer().getCurrentAd().getTitle();
    }

    //Not possbiel to get
    /*@Override
    public String getResource() {
        //return adTag != null ? adTag : super.getResource();
    }*/

    @Override
    public String getPlayerVersion() {
        return "IMA"/*+ "3.6.0"*/; //No way to get it from the code, so the hardcoded number of gradle is used instead
    }

    @Override
    public String getPlayerName() {
        return "IMA";
    }

    @Override
    public AdPosition getPosition() {

        if(getPlayer().getCurrentAd() == null)
            return AdPosition.UNKNOWN;

        int pos = getPlayer().getCurrentAd().getAdPodInfo().getPodIndex();

        if(pos == 0)
            return  AdPosition.PRE;

        if(pos == -1)
            return AdPosition.POST;

        if(pos > 0)
            return AdPosition.MID;

        return AdPosition.UNKNOWN;
    }

    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-IMA";
    }

    public void setAdTag(String adTag){
        this.adTag = adTag;
    }

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        String message = adErrorEvent.getError().getMessage();
        int code = adErrorEvent.getError().getErrorCode().getErrorNumber();
        fireError(message,String.valueOf(code),null);
    }

    @Override
    public void onAdEvent(AdEvent adEvent) {
        switch (adEvent.getType()) {
            case LOADED:
                fireStart();
                break;
            case CONTENT_PAUSE_REQUESTED:
                adServed = true;
                fireStart();
                break;
            case STARTED:
                fireJoin();
                break;
            case PAUSED:
                firePause();
                break;
            case RESUMED:
                fireResume();
                break;
            case TAPPED:
                break;
            case CLICKED:
                //AD url it's there on ad information but no way to retrieve it
                fireClick(getUrl());
                break;
            case CONTENT_RESUME_REQUESTED:
            case COMPLETED:
                lastPosition = lastPosition == AdPosition.POST ? AdPosition.POST : getPosition();

                if(!adServed){
                    HashMap<String, String> notServerdErrorParams = new HashMap<>();
                    notServerdErrorParams.put("errorCode","AD_NOT_SERVED");
                    notServerdErrorParams.put("errorMsg","Ad not served");
                    notServerdErrorParams.put("errorSeverity","AdsNotServed");
                    fireError(notServerdErrorParams);
                }
                //IMA "resets" playhead before firing this event so we send duration instead
                fireStop(new HashMap<String, String>() {{
                    put("adPlayhead",String.valueOf(getDuration() == null ? 0.0 : getDuration()));
                }});
                break;
            case SKIPPED:
                fireSkip();
                break;
            case ALL_ADS_COMPLETED:
                fireAllAdsCompleted();
                break;
            case LOG:
                if(adEvent.getAdData().get("errorCode") != null && !adEvent.getAdData().get("errorCode").equals("1009")){
                    fireError(adEvent.getAdData().get("errorMessage"),adEvent.getAdData().get("errorCode"),null);
                    getPlugin().getOptions().setAdsAfterStop(0);
                }
            default:
                break;
        }
    }
}
