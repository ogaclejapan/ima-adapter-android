package com.npaw.ima;

import com.google.ads.interactivemedia.v3.api.Ad;
import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.StreamManager;
import com.npaw.ima.BuildConfig;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.adapter.PlayerAdapter;

import java.util.HashMap;
import java.util.Map;

public class ImaDAIAdapter extends PlayerAdapter<StreamManager> implements AdErrorEvent.AdErrorListener,
        AdEvent.AdEventListener {

    private double adProgress;

    private Ad currentAd;
    private boolean hasStarted;

    /**
     * Constructor.
     * When overriding it, make sure to call super.
     * Implement the logic to register to the player events in the {@link #registerListeners()} method.
     * Usually you will want to call {@link #registerListeners()} from the overridden constructor.
     *
     * @param player the player instance this Adapter will be bounded to.
     */
    public ImaDAIAdapter(StreamManager player) {
        super(player);
        hasStarted = false;
        adProgress = 0;
        registerListeners();
        monitorPlayhead(false,false,800);
    }

    @Override
    public void registerListeners() {
        super.registerListeners();

        if(getPlayer() == null) return;

        getPlayer().addAdErrorListener(this);
        getPlayer().addAdEventListener(this);
    }

    @Override
    public void unregisterListeners() {
        super.unregisterListeners();

        getPlayer().removeAdErrorListener(this);
        getPlayer().removeAdEventListener(this);
    }

    @Override
    public Double getPlayhead() {
        return (double)adProgress;
    }

    @Override
    public Double getDuration() {
        return currentAd == null ? -1 :  currentAd.getDuration();
    }

    @Override
    public String getTitle() {
        return currentAd == null ? super.getTitle() : currentAd.getTitle();
    }

    //Not possbiel to get
    /*@Override
    public String getResource() {
        //return adTag != null ? adTag : super.getResource();
    }*/

    @Override
    public String getPlayerVersion() {
        return "IMA"/*+ "3.6.0"*/; //No way to get it from the code, so the hardcoded number of gradle is used instead
    }

    @Override
    public String getPlayerName() {
        return "IMA";
    }

    @Override
    public AdPosition getPosition() {

        if(currentAd == null || getDuration() == null)
            return AdPosition.UNKNOWN;

        if(getPlugin() != null && getPlugin().getIsLive() != null && getPlugin().getIsLive().equals(Boolean.TRUE))
            return AdPosition.MID;

        double offset = currentAd.getAdPodInfo().getTimeOffset();

        if(offset == 0.0){
            return AdPosition.PRE;
        }
        if(getPlugin().getAdapter() != null && offset == Math.round(getPlugin().getAdapter().getDuration()) - Math.round(getDuration())){
            return AdPosition.POST;
        }
        return AdPosition.MID;
    }

    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME + "-IMA";
    }

    @Override
    public void fireStart() {
        if(getPlugin() != null && getPlugin().getAdapter() != null
                && !getPlugin().getAdapter().getFlags().isJoined()
                && (getPosition() == AdPosition.PRE || (getPlugin().getIsLive() != null
                && getPlugin().getIsLive().equals(Boolean.TRUE)))){
            getPlugin().getAdapter().fireJoin();
        }
        super.fireStart();
    }

    @Override
    public void fireStop(Map<String, String> params) {
        super.fireStop(params);
        currentAd = null;
        adProgress = 0;
    }

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {
        String message = adErrorEvent.getError().getMessage();
        int code = adErrorEvent.getError().getErrorCode().getErrorNumber();
        fireError(message,String.valueOf(code),null);
    }

    @Override
    public void onAdEvent(AdEvent adEvent) {
        YouboraLog.debug("Ad event: "+adEvent.getType().toString());
        if(adEvent.getAd() != null){
            currentAd = adEvent.getAd();
        }
        switch (adEvent.getType()) {
            case STARTED:
                hasStarted = true;
                if(getFlags().isStarted()){
                    fireStop();
                }
                        /*if(getPlugin() != null && getPlugin().getAdapter()!= null && !getPlugin().getAdapter().getFlags().isJoined()
                                && getPosition() == AdPosition.PRE){
                            getPlugin().getAdapter().fireJoin();
                        }*/
                fireStart();
                break;
            case PAUSED:
                //firePause();
                break;
            case RESUMED:
                //fireResume();
                break;
            case TAPPED:
                break;
            case CLICKED:
                //AD url it's there on ad information but no way to retrieve it
                fireClick();
                break;
            case COMPLETED:
                //IMA "resets" playhead before firing this event so we send duration instead
                fireStop(new HashMap<String, String>(){
                    {put("adPlayhead",String.valueOf(getDuration() == null ? 0.0 : getDuration()));}
                });
                break;
            case SKIPPED:
                fireSkip();
                break;
            case ALL_ADS_COMPLETED:
                fireAllAdsCompleted();
                break;
            case LOG:
                if(adEvent.getAdData().get("errorCode") != null && !adEvent.getAdData().get("errorCode").equals("1009")){
                    fireError(adEvent.getAdData().get("errorMessage"),adEvent.getAdData().get("errorCode"),null);
                    getPlugin().getOptions().setAdsAfterStop(0);
                }
                break;
            case AD_PROGRESS:
                adProgress = getPlayer().getAdProgressInfo().getCurrentTime();
                fireJoin();
                break;
            case FIRST_QUARTILE:
                break;
            case MIDPOINT:
                break;
            case THIRD_QUARTILE:
                break;
            default:
                break;
        }
    }
}
